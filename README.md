# Viva la ideia #

### Tecnologias ###

* Bootstrap
* Stylus
* CSS3
* HTML5
* Javascript
* jQuery
* Gulp

### Rodando o gulp  ###
Instalando o gulp [clique aqui](https://github.com/gulpjs/gulp/blob/master/docs/getting-started.md).
depois de instalar o gulp em sua maquina entre no diretorio ***src*** abra seu terminal e execute 
```
#!shell

npm install
```
esse comando ira instalar todos 
os modulos que se encontram no package.json.


### Iniciando o projeto ###

Dentro do terminal execute o comando abaixo.

```
#!shell

gulp
```
Esse comando iriar criar todo o nosso workflow dentro da pasta ***public***

### Plugins do Gulp ###
Aqui esta a lista de plugins que estão instalados no projeto.

* browser-sync - 2.2.2    
* gulp-changed 1.1.1
* gulp-concat - 2.5.2
* gulp-imagemin - 2.2.1
* gulp-minify-css - 0.5.1
* gulp-notify - 2.2.0
* gulp-stylus - 2.0.1
* gulp-uglify - 1.1.0
