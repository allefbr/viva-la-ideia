<?php include('header.php'); ?>
    <main>

        <a href="#quem somos" data-scroll-nav="1" class="icon-seta-top">teste</a>
        <!-- painel topo -->
        <section id="slides" data-scroll-index="0">
            <ul class="slides-container">
                <li>
                    <img src="public/assets/img/painel/01.jpg" alt="Imagem painel">
                </li>

                <li>
                    <img src="public/assets/img/painel/02.jpg" alt="Imagem painel">
                </li>
            </ul>
        </section>

        <section id="quemsomos" data-scroll-index="1" class="quem-somos" >
            <div class="container">
                <div class="row">
                    <div class="col-sm-9 col-sm-offset-2 col-md-5 col-md-offset-0">
                        <img src="public/assets/img/img-quemsomos.png" alt="#" class="img-full" />
                    </div>
                    <div class="col-sm-12 col-md-7">
                        <h2 class="tit1">Seu negócio</h2>
                        <h2 class="tit2">Merece boas ideias</h2>

                        <p class="tit-p">We are also committed to helping people discover their gifts and calling, for the purpose of glorifying God in the world. As people surrender their lives to Jesus, God, through the power of His Spirit, invites them into the process He has already begun of creating a new world in which everything will be restored to perfect harmony, peace, and the goodness of His original intent.</p>
                        <p class="tit-p">We are also committed to helping people discover their gifts and calling, for the purpose of glorifying God in the world. As people surrender their lives to Jesus, God, through the power of His Spirit, invites them into the process He has already begun of creating a new world in which everything will be restored to perfect harmony, peace, and the goodness of His original intent.</p>
                    </div>
                </div>

                <div class="row">
                    <div>
                        <nav class="nav-cicle text-center">
                            <a href="#" class="bt-cicle bt-red">Baixe nossa apresentação</a>
                            <span>ou</span>
                            <a href="#" class="bt-cicle bt-green">Nos envie sua idea</a>
                        </nav>
                    </div>
                </div>
            </div>
        </section>

        <div class="parallax" data-speed="15"></div>


        <section id="clientes" data-scroll-index="2" class="clientes">
            <div class="container">
                <div class="row">
                    <h2 class="tit text-center">Clientes</h2>
                    <h3 class="sub-tit text-center">você pode ser um cliente da Viva la Idea, <a href="#" class="link-p">clicando aqui</a></h3>
                </div>
                <div class="row">
                    <ul class="box-clientes">
                        <li class="item col-sm-6 col-md-3">
                            <a class="link" href="#">
                                <img src="public/assets/img/clientes/01.jpg" alt="#" class="img-full grayscale"/>
                            </a>
                        </li>

                        <li class="item col-sm-6 col-md-3">
                            <a class="link" href="#">
                                <img src="public/assets/img/clientes/02.jpg" alt="#" class="img-full grayscale"/>
                            </a>
                        </li>

                        <li class="item col-sm-6 col-md-3">
                            <a class="link" href="#">
                                <img src="public/assets/img/clientes/03.jpg" alt="#" class="img-full grayscale"/>
                            </a>
                        </li>

                        <li class="item col-sm-6 col-md-3">
                            <a class="link" href="#">
                                <img src="public/assets/img/clientes/01.jpg" alt="#" class="img-full grayscale"/>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </section>

        <section id="servicos" data-scroll-index="3" class="servicos">
            <div class="container">
                <div class="row">
                    <h2 class="tit text-center">Serviços</h2>
                    <h3 class="sub-tit text-center">você pode ser um cliente da Viva la Idea, <a href="#" class="link-p">clicando aqui</a></h3>
                </div>

                <div class="row">
                    <div class="col-sm-5 col-md-4 box-serv">
                        <div class="bg-red">
                            <h2 class="serv-tit text-center">Marketing <br/> digital</h2>
                            <p class="serv-desc">As mídias digitais estão em constante crescimento e sua marca deve preparar-se para atuar no meio online. Com uma completa medição de resultados, é possível atingir seu público-alvo, garantindo a melhor relação custo-benefício.</p>
                        </div>
                    </div>

                    <div class="col-sm-7 col-md-8 box-serv">
                        <div class="bg-darkBlue">
                            <h2 class="serv-tit text-center">Sites e lojas virtuais </h2>
                            <p class="serv-desc">Projetamos e desenvolvemos sites, portais, hotsites, blogs, entre outros serviços, garantindo a presença online da sua marca.</p>
                            <p class="serv-desc">Planejamos e desenvolvemos lojas online com sistemas para pagamento seguro e tráfego de dados criptografados (site seguro).</p>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-6 col-md-4 box-serv">
                        <div class="bg-blue">
                            <h3 class="serv-tit-g">Hospedagem de site</h3>
                        </div>
                    </div>

                    <div class="col-sm-6 col-md-4 box-serv">
                        <div class="bg-orange">
                            <h3 class="serv-tit-g">Hospedagem de site</h3>
                        </div>
                    </div>

                    <div class="col-sm-6 col-md-4 box-serv">
                        <div class="bg-green">
                            <h3 class="serv-tit-g">Hospedagem de site</h3>
                        </div>
                    </div>
                </div>
            </div>
        </section>


        <section class="whatsapp">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-6 col-md-7">
                        <h2 class="tit-whats">Queremos <br />ser mais próximos.</h2>
                        <p>We are also committed to helping people discover their gifts and calling.</p>
                        <p>Deixe seu número conosco, <a href="#" class="link-p">clique aqui</a></p>
                    </div>

                    <aside class="hidden-xs col-sm-6 col-md-5">
                      <img src="public/assets/img/whatapp.png" alt="" class="img-full"/>
                    </aside>
                </div>
            </div>
        </section>

        <section id="trabalhos" data-scroll-index="4" class="trabalhos">
            <!-- painel topo -->
            <section id="slides2" data-scroll-index="0">
                <ul class="slides-container">
                    <li>
                        <img src="public/assets/img/trabalhos/01.jpg" alt="Imagem painel">
                        <div class="carousel-caption">
                            <div class="container">
                                <div class="row">
                                    <div class="col-sm-6 col-md-5">
                                        <h2 class="tit-trab">Novos Clicks</h2>
                                        <h3 class="sub-tit-trab">para um fotografo</h3>

                                        <p class="tit-p">We are committed to being and making disciples. Disciples are people who are so in love with Jesus, they have decided that the most important thing in their lives is to learn how to do what Jesus said to do.</p>
                                        <a class="bt-cicle bt-red" href="#">ver site</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>

                    <li>
                        <img src="public/assets/img/trabalhos/01.jpg" alt="Imagem painel">
                        <div class="carousel-caption">
                            <div class="container">
                                <div class="row">
                                    <div class="col-sm-6 col-md-5">
                                        <h2 class="tit-trab">Novos Clicks</h2>
                                        <h3 class="sub-tit-trab">para um fotografo</h3>

                                        <p class="tit-p">We are committed to being and making disciples. Disciples are people who are so in love with Jesus, they have decided that the most important thing in their lives is to learn how to do what Jesus said to do.</p>
                                        <a class="bt-cicle bt-red" href="#">ver site</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                </ul>

                <!-- Passador -->
                <nav class="slides-navigation">
                    <a href="#" class="next">Next</a>
                    <a href="#" class="prev">Previous</a>
                </nav>
            </section>

            <div class="container-fluid">
                <div class="row">
                    <h2 class="tit text-center"> Mais trabalhos</h2>

                    <ul id="list-trab" class="list-trabalhos grid">
                        <li class="item col-sm-6 col-md-3">
                            <figure class="effect-milo">
                                <img src="public/assets/img/trabalhos/thumb1.jpg" alt="img11"/>
                                <figcaption>
                                    <p>Milo went to the woods. He took a fun ride and never came back.</p>
                                    <a href="maistrabalhos.php" class="bt-ajax">View more</a>
                                </figcaption>
                            </figure>
                         </li>

                        <li class="item col-sm-6 col-md-3">
                            <figure class="effect-milo">
                                <img src="public/assets/img/trabalhos/thumb1.jpg" alt="img11"/>
                                <figcaption>
                                    <p>Milo went to the woods. He took a fun ride and never came back.</p>
                                    <a href="maistrabalhos.php" class="bt-ajax">View more</a>
                                </figcaption>
                            </figure>
                         </li>

                        <li class="item col-sm-6 col-md-3">
                            <figure class="effect-milo">
                                <img src="public/assets/img/trabalhos/thumb1.jpg" alt="img11"/>
                                <figcaption>
                                    <p>Milo went to the woods. He took a fun ride and never came back.</p>
                                    <a href="maistrabalhos.php" class="bt-ajax">View more</a>
                                </figcaption>
                            </figure>
                         </li>

                        <li class="item col-sm-6 col-md-3">
                            <figure class="effect-milo">
                                <img src="public/assets/img/trabalhos/thumb1.jpg" alt="img11"/>
                                <figcaption>
                                    <p>Milo went to the woods. He took a fun ride and never came back.</p>
                                    <a href="maistrabalhos.php" class="bt-ajax">View more</a>
                                </figcaption>
                            </figure>
                         </li>
                    </ul>
                </div>

                <div id="box-content" class="row">
                    <div class="box-load">
                        <div class="box-cont-ajax"></div>
                    </div>
                </div>

            </div>
        </section>

        <section id="contato" data-scroll-index="5" class="contato">
            <div class="container">
                <div class="row">
                    <h2 class="tit text-center">Você vai adorar nossas ideas</h2>
                    <h3 class="sub-tit text-center">Entre em contato e veja como  é fácil diviulgar seu serviço</h3>

                    <form class="form">
                        <div class="form-menor">
                            <div class="form-group">
                                <input type="email" class="form-control" id="nome" placeholder="Nome">
                            </div>

                            <div class="form-group">
                                <input type="email" class="form-control" id="email" placeholder="E-mail">
                            </div>

                            <div class="form-group">
                                <input type="phone" class="form-control" id="fone" placeholder="Telefone">
                            </div>

                            <div class="form-group">
                                <textarea name="mensagem" id="mensagem" class="form-control txtarea" placeholder="Mensagem(opcional)"></textarea>
                            </div>
                        </div>

                        <button type="submit" class="btn bt-full-cicle">Enviar</button>
                    </form>
                </div>
            </div>
        </section>
    </main>
<?php include('footer.php'); ?>