<!doctype html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <link rel="shortcut icon" href="public/assets/img/favicon.ico" type="image/x-icon">
    <link rel="icon" href="public/assets/img/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" href="public/assets/css/style.min.css" />
    <link href='http://fonts.googleapis.com/css?family=Raleway:400,800,300' rel='stylesheet' type='text/css'>
    <meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=no">
    <title>Viva la ideia</title>
</head>
<body>
    <!--[if lt IE 8]>
    <p class="browserupgrade">Você está usando um navegador <strong>desatualizado</strong>.  Por favor <a href="http://browsehappy.com/">atualize seu navegador</a> para melhorar a sua experiência.</p>
    <![endif]-->
    <header class="topo">
        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#nav-responsive">
                        <span class="sr-only">Menu escondido</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar hover-space"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" data-scroll-nav="0" href="#"><img src="public/assets/img/logo.png" alt="" class="full"/></a>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="nav-responsive">
                    <ul class="nav navbar-nav">
                        <li><a href="#" data-scroll-nav="0">Inicio</a></li>
                        <li><a href="#quem somos" data-scroll-nav="1">Quem somos</a></li>
                        <li><a href="#clientes" data-scroll-nav="2">Clientes</a></li>
                        <li><a href="#servicos" data-scroll-nav="3">Serviços</a></li>
                        <li><a href="#trabalhos" data-scroll-nav="4">Trabalhos</a></li>
                        <li><a href="#contatos" data-scroll-nav="5">Contatos</a></li>
                    </ul>
                </div><!-- /.navbar-collapse -->
            </div><!-- /.container-fluid -->
        </nav>
    </header>
</body>
</html>