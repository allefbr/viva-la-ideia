jQuery( function($){
    // Carousel
    $("#owl-example").owlCarousel({
        items : 3,
        autoPlay : false
    });

    // Slide
    $('#slides').superslides({
        animation: 'fade'
    });

    // Slide2
    $('#slides2').superslides();

    // scroll
    $.scrollIt({
        topOffset: -85
    });

    // Menu fixo
    quem = $("#quemsomos").offset().top;
    $(window).scroll( function (){
        ws = $(window).scrollTop();

        if ( ws >= quem - 85) {
            $(".navbar-default").addClass("menu-fixo");
        } else {
            $(".navbar-default").removeClass("menu-fixo");
        }
    });

    // parallax
    $('.parallax').each(function(){
        var $obj = $(this);

        $(window).scroll(function() {
            var yPos = -($(window).scrollTop() / $obj.data('speed'));

            var bgpos = '50% '+ yPos + 'px';

            $obj.css('background-position', bgpos );
        });
    });

    // Tooltips
    $('[data-toggle="tooltip"]').tooltip().show();

    //Bootsmenu
    var largW = $(window).innerWidth();
    if ( largW <= 768 ){
        $(".nav a").click(function(){
          $(".navbar-collapse").removeClass('in');
        });
    }

});



// Ir para Topo
function goToByScroll(id){
    $('html,body').animate({
        scrollTop: $( "#"+id ).offset().top -83
    }, 500 );
}

// colecaoo
jQuery(function ($) {
    $('.bt-ajax').on('click', function (e){
        e.preventDefault();
        anc = $(this).attr('href');
        $('.bt-ajax').removeClass('marc');
        $(this).addClass('marc');
        goToByScroll('box-content');

        $('.box-cont-ajax').hide().load(anc+' .box-int-pac', function (){
            $(this).slideDown();

            $("#owl-example").owlCarousel({
                items : 2,
                autoPlay : false
            });

            jQuery('.close').on('click', function (e){
                e.preventDefault();
                $('.box-int-pac').slideUp();
                $('.bt-ajax').removeClass('marc');
                goToByScroll('list-trab');
            });
        });
    });
});