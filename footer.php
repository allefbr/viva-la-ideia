        <footer class="bg-black">
            <div class="rdp container">
                <div class="row">
                    <div class="col-sm-2 col-md-1 pad-reset">
                        <img class="img-full" src="public/assets/img/logo-rdp.png" alt=""/>
                    </div>

                    <div class="col-sm-6 col-md-7">
                        <address class="tit-end">
                            Todos os direitos reservados para Viva la Idea. Rua Seringueia, 45 - Cajazerias - Fortaleza/CE<br />
                            contato@vivalaidea.com.br | +55 85 4141.4026
                        </address>
                    </div>

                    <div class="col-sm-4 col-md-3 col-md-offset-1 pd-reset">
                        <ul class="box-midias pull-right">
                            <li><a href="#" class="facebook">Facebook</a></li>
                            <li><a href="#" class="instagram">Instagram</a></li>
                            <li><a href="#" class="linkedin">Linkedin</a></li>
                            <li><a href="#" class="icon-whatsapp" data-toggle="tooltip" data-placement="top" title="4141.4042">Whatsapp</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </footer>

        <!-- Libs -->
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="./public/assets/js/vendor/jquery-1.11.2.min.js"><\/script>')</script>
        <script src="./assets/js/vendor/html5shiv.min.js"></script> 
        
        <!-- Custom scripts -->       
        <script src="./public/assets/js/script.min.js"></script>


        <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
        <script>
            (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
                function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
                e=o.createElement(i);r=o.getElementsByTagName(i)[0];
                e.src='//www.google-analytics.com/analytics.js';
                r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
            ga('create','UA-XXXXX-X','auto');ga('send','pageview');
        </script>
    </body>
</html>