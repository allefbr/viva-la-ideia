<?php include('header.php'); ?>
<main>
    <section class="box-int-pac">
        <div class="col-md-3 pd-reset">
            <div>
                <span class="close">x</span>
                <h2 class="tit-trab">Novos Clicks</h2>
                <h3 class="sub-tit-trab">para um fotografo</h3>

                <p class="tit-p">We are committed to being and making disciples. Disciples are people who are so in love with Jesus, they have decided that the most important thing in their lives is to learn how to do what Jesus said to do.</p>
                <a class="bt-cicle bt-red" href="#">ver site</a>
            </div>
        </div>

        <div class="col-md-9 pad-reset">
            <div id="owl-example" class="owl-carousel">
                <div><img src="public/assets/img/trabalhos/thumb1.jpg" alt=""></div>
                <div><img src="public/assets/img/trabalhos/thumb1.jpg" alt=""></div>
                <div><img src="public/assets/img/trabalhos/thumb1.jpg" alt=""></div>
                <div><img src="public/assets/img/trabalhos/thumb1.jpg" alt=""></div>
                <div><img src="public/assets/img/trabalhos/thumb1.jpg" alt=""></div>
                <div><img src="public/assets/img/trabalhos/thumb1.jpg" alt=""></div>
            </div>
        </div>
    </section>
</main>
<?php include('footer.php'); ?>