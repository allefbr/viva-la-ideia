// Diretorio da base
var path = { 
    dest  : '../public/',
    local : 'localhost/viva-la-ideia/_html/'
}

// Carregando modulos
var gulp = require('gulp'),
    stylus = require('gulp-stylus'),
    minifyCss = require('gulp-minify-css'),
    uglify = require('gulp-uglify'),
    concat = require('gulp-concat'),
    imagemin = require('gulp-imagemin'),
    changed  = require('gulp-changed'),
    browserSync = require('browser-sync'),
    notify = require("gulp-notify");

// Compila Stylus para CSS
gulp.task('stylus', function(){
    gulp.src('../assets/stylus/**/*.styl')
        .pipe( stylus() )
        .pipe( notify('CSS compilado!') )
        .pipe( gulp.dest( '../assets/css') )
});

// Concatena o CSS
gulp.task('css', function() {
    return gulp.src('../assets/css/*.css')
        .pipe( concat('style.min.css') )
        .pipe( gulp.dest( '../assets/.tmp/') );
});

// Otimiza o CSS
gulp.task('cssMin', ['stylus', 'css'], function(){
    gulp.src('../assets/.tmp/*.css')
        .pipe( minifyCss() )
        .pipe( gulp.dest( path.dest + 'assets/css/') )
});

// Imagens Otimizadas
gulp.task('img', function() {
    gulp.src('../assets/img/**/*')
        .pipe(changed( path.dest + 'assets/img'))
        .pipe(imagemin({
            progressive: true,
            svgoPlugins: [{removeViewBox: false}]
        }))
        .pipe(gulp.dest( path.dest + 'assets/img'));
});

// Copia os vendor
gulp.task('jsVendor', function(){
    gulp.src('../assets/js/vendor/*.js')
        .pipe( changed( path.dest + 'assets/js') )
        .pipe( gulp.dest( path.dest + 'assets/js') );
});

// Concatenando os arquivos
gulp.task('scripts', function() {
  return gulp.src(['../assets/js/_bootstrap.min.js', '../assets/js/_owl.carousel.min.js', '../assets/js/jquery.superslides.min.js', '../assets/js/_scrollIt.min.js', '../assets/js/main.js' ])
    .pipe( concat('script.min.js') )
    .pipe( gulp.dest( '../assets/.tmp/') );
});

// Mimifica o javascript
gulp.task('js', ['scripts'], function() {
  gulp.src('../assets/.tmp/*.js')
    .pipe( uglify() )
    .pipe( gulp.dest( path.dest + 'assets/js') )
});


// Copia as fontes
gulp.task('fonts', function(){
    gulp.src('../assets/fonts/*')
        .pipe( changed( path.dest + 'assets/fonts') )
        .pipe( gulp.dest( path.dest + 'assets/fonts') );
});

// Browser-sync
gulp.task('browser-sync', function() {
    browserSync.init( [ path.dest + 'assets/css/*.css', path.dest + '/img/**', '../assets/stylus/**/*.styl', '../*.php' ], {
        proxy: path.local
    });
});

// Observa os arquivos
gulp.task('watch', ['cssMin', 'js', 'browser-sync'], function(){
    gulp.watch( '../assets/stylus/**/*.styl', ['stylus']);
    gulp.watch( '../assets/css/**/*.css', ['cssMin']);
});

// Default
gulp.task( 'default', ['cssMin', 'js', 'img', 'fonts']);